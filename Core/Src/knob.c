/*
 * knob.c
 *
 *  Created on: Jan 1, 2024
 *      Author: jojo
 */


#include "knob.h"

#include <stdint.h>
#include <stdbool.h>


int8_t evaluate_knob(bool a, bool b)
{
	typedef enum {
		KNOB_OUTPUT_NONE = 0,
		KNOB_OUTPUT_A,
		KNOB_OUTPUT_B,
	} KnobOutput;

	static bool had_both = false;
	static KnobOutput leader = KNOB_OUTPUT_NONE;

	int8_t res = 0;

	if (a && b) {
		had_both = true;
		leader = KNOB_OUTPUT_NONE;
	}
	else if (!a && !b) {
		if (had_both) {
			had_both = false;
			if (KNOB_OUTPUT_A == leader) {
				res = -1;
			}
			else if (KNOB_OUTPUT_B == leader) {
				res = 1;
			}
		}
		leader = KNOB_OUTPUT_NONE;
	}
	else if (a) {
		if (had_both) {
			leader = KNOB_OUTPUT_B;
		}
	}
	else if (b) {
		if (had_both) {
			leader = KNOB_OUTPUT_A;
		}
	}

	return res;
}
