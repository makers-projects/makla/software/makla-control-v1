/*
 * buttons.c
 *
 *  Created on: Dec 3, 2023
 *      Author: jojo
 */


#include "buttons.h"

#include "stm32f3xx_hal.h"

#include <stdint.h>

#define NUM_BTN_SREGS 4
#define NUM_BTNS (NUM_BTN_SREGS * 8)


static void sreg_btn_init(void)
{
	HAL_GPIO_WritePin(GPIO_BTN_CLK, PIN_BTN_CLK, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIO_BTN_DATA, PIN_BTN_DATA, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIO_BTN_LATCH, PIN_BTN_LATCH, GPIO_PIN_RESET);
}

static void sreg_btn_latch(void)
{
	HAL_GPIO_WritePin(GPIO_BTN_LATCH, PIN_BTN_LATCH, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIO_BTN_LATCH, PIN_BTN_LATCH, GPIO_PIN_RESET);
}

static void sreg_btn_shift_byte(uint8_t regval)
{
	for (uint8_t i = 0; i < 8; i++) {
		HAL_GPIO_WritePin(GPIO_BTN_DATA, PIN_BTN_DATA, ((regval >> i) & 1) ? GPIO_PIN_SET : GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIO_BTN_CLK, PIN_BTN_CLK, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIO_BTN_CLK, PIN_BTN_CLK, GPIO_PIN_RESET);
	}
}

static void btn_activate(void)
{
	for (uint8_t nr_sreg = 0; nr_sreg < NUM_BTN_SREGS; nr_sreg ++) {
		sreg_btn_shift_byte(0xff);
	}
	sreg_btn_latch();
}

static void btn_clear(void)
{
	for (uint8_t nr_sreg = 0; nr_sreg < NUM_BTN_SREGS; nr_sreg ++) {
		sreg_btn_shift_byte(0x00);
	}
	sreg_btn_latch();
}


void btn_init(void)
{
	sreg_btn_init();
	HAL_Delay(100);
	btn_activate();
}

uint32_t btn_read(void)
{
	btn_clear();

	uint32_t res = 0;

	HAL_GPIO_WritePin(GPIO_BTN_DATA, PIN_BTN_DATA, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIO_BTN_CLK, PIN_BTN_CLK, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIO_BTN_CLK, PIN_BTN_CLK, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIO_BTN_DATA, PIN_BTN_DATA, GPIO_PIN_RESET);

	for (uint8_t nr_btn = 0; nr_btn < NUM_BTNS; nr_btn ++) {
		sreg_btn_latch();

		if (GPIO_PIN_SET == HAL_GPIO_ReadPin(GPIO_BTN_INPUT, PIN_BTN_INPUT)) {
			res |= (1 << nr_btn);
		}

		HAL_GPIO_WritePin(GPIO_BTN_CLK, PIN_BTN_CLK, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIO_BTN_CLK, PIN_BTN_CLK, GPIO_PIN_RESET);

		/*
		for (uint8_t nr_sreg = 0; nr_sreg < NUM_BTN_SREGS; nr_sreg ++) {
			uint8_t regval = 0;
			sreg_btn_shift_byte
		}
		*/
	}

	btn_activate();

	return res;
}
