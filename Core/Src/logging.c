/*
 * logging.c
 *
 *  Created on: Nov 16, 2023
 *      Author: jojo
 */

#include "logging.h"

#include "usart.h"

#include <stdio.h>
#include <stdarg.h>

#define LOGLEVEL_BASELINE 0
#define LOGLEVEL_ERROR 1
#define LOGLEVEL_WARNING 2
#define LOGLEVEL_DEBUG 3
#define LOGLEVEL_NOISY 4

#ifndef LOGLEVEL
#define LOGLEVEL LOGLEVEL_NOISY
#endif

#define LOGGING_ACTIVE 1




int _write(int file, char *ptr, int len)
{
	HAL_UART_Transmit(&huart2, (uint8_t*)ptr, len, HAL_MAX_DELAY);
	return len;
}


void log_baseline(const char *formatstr, ...) {
#if 1 == LOGGING_ACTIVE
    va_list args;
	va_start(args, formatstr);
	vprintf(formatstr, args);
	printf("\r\n");
	va_end(args);
#endif
}

void log_error(const char *formatstr, ...) {
#if 1 == LOGGING_ACTIVE && LOGLEVEL >= LOGLEVEL_ERROR
    va_list args;
	va_start(args, formatstr);
	printf("ERROR: ");
	vprintf(formatstr, args);
	printf("\r\n");
	va_end(args);
#endif
}

void log_warning(const char *formatstr, ...) {
#if 1 == LOGGING_ACTIVE && LOGLEVEL >= LOGLEVEL_WARNING
    va_list args;
	va_start(args, formatstr);
	printf("WARNING: ");
	vprintf(formatstr, args);
	printf("\r\n");
	va_end(args);
#endif
}

void log_debug(const char *formatstr, ...) {
#if 1 == LOGGING_ACTIVE && LOGLEVEL >= LOGLEVEL_DEBUG
    va_list args;
	va_start(args, formatstr);
	vprintf(formatstr, args);
	printf("\r\n");
	va_end(args);
#endif
}

void log_noisy(const char *formatstr, ...) {
#if 1 == LOGGING_ACTIVE && LOGLEVEL >= LOGLEVEL_NOISY
    va_list args;
	va_start(args, formatstr);
	vprintf(formatstr, args);
	printf("\r\n");
	va_end(args);
#endif
}
