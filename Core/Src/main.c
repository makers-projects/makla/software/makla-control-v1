/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "spi.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "sseg.h"
#include "buttons.h"
#include "knob.h"
#include "settings.h"
#include "logging.h"

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */



static volatile bool interrupt_knob = false;
static volatile bool interrupt_btn = false;


void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    if(PIN_BTN_INPUT == GPIO_Pin) {
    	interrupt_btn = true;
    }
    else if(PIN_KNOB_A == GPIO_Pin || PIN_KNOB_B == GPIO_Pin) {
    	interrupt_knob = true;
    }
}

void blink(uint8_t num) {
	for (int32_t i = 0; i < num; i ++) {
		HAL_GPIO_WritePin(GPIOF, GPIO_PIN_0, GPIO_PIN_SET);
		HAL_Delay(80);
		HAL_GPIO_WritePin(GPIOF, GPIO_PIN_0, GPIO_PIN_RESET);
		HAL_Delay(200);
	  }
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_SPI1_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */

  log_baseline("=== STARTING ===");

  sseg_init();
  HAL_Delay(100);
  btn_init();
  HAL_Delay(500);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  bool obtained = settings_obtain_complete_all();
  if (!obtained) {
	  sseg_set_string("Error\nfetching\nsettings");
	  log_error("Could not obtain settings");
	  while (1);
  }
  settings_show();

  uint32_t systime_last_knob_interrupt = 0;
  uint32_t systime_last_btn_interrupt = 0;
  uint32_t systime_last_setting_change = 0;

  log_baseline("=== ENTERING MAIN LOOP ===");

  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  if (interrupt_knob) {
	  	  		  interrupt_knob = false;
	  	  		  systime_last_knob_interrupt = HAL_GetTick();
	  	  	  }

	  	  	  if (interrupt_btn) {
	  	  		  interrupt_btn = false;
	  	  		  systime_last_btn_interrupt = HAL_GetTick();
	  	  	  }

	  	  	  if (0 < systime_last_knob_interrupt && HAL_GetTick() - systime_last_knob_interrupt > 0) {
	  	  		  systime_last_knob_interrupt = 0;
	  	  		  bool a = (HAL_GPIO_ReadPin(GPIO_KNOB_A, PIN_KNOB_A) == GPIO_PIN_SET);
	  	  		  bool b = (HAL_GPIO_ReadPin(GPIO_KNOB_B, PIN_KNOB_B) == GPIO_PIN_SET);
	  	  		  int8_t knob_change = evaluate_knob(a, b);
	  	  		  bool changed = false;
	  	  		  if (0 > knob_change) {
	  	  			  changed = settings_decrement_current();
	  	  		  }
	  	  		  else if (0 < knob_change) {
	  	  			  changed = settings_increment_current();
	  	  		  }
	  	  		  if (changed) {
	  	  			  settings_show();
	  	  			  systime_last_setting_change = HAL_GetTick();

	  	  		  }
	  	  	  }

	  	  	  if (0 < systime_last_btn_interrupt && HAL_GetTick() - systime_last_btn_interrupt > 10) {
	  	  		  systime_last_btn_interrupt = 0;
	  	  		  uint32_t btns_pressed = btn_read();
	  	  		  interrupt_btn = false;

	  	  		  if (0 < systime_last_setting_change) {
	  	  			  systime_last_setting_change = 0;
		  	  		  settings_send_current();
	  	  		  }

	  	  		  bool changed = false;
	  	  		  for (uint8_t i = 0; i < 32; i ++) {
	  	  			  if ((btns_pressed >> i) & 0x01) {
	  	  				  changed = settings_select(i);
	  	  			  }
	  	  		  }
	  	  		  if (changed) {
	  	  			  settings_show();
	  	  		  }
	  	  	  }

	  	  	  if (0 < systime_last_setting_change && HAL_GetTick() - systime_last_setting_change > 1000) {
	  	  		  systime_last_setting_change = 0;
	  	  		  settings_send_current();
	  	  	  }
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
