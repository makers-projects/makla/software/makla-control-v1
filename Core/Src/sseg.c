/*
 * sseg.c
 *
 *  Created on: Dec 3, 2023
 *      Author: jojo
 */

#include "sseg.h"

#include "stm32f3xx_hal.h"

#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include <string.h>

#define	GPIO_SSEG_CLK GPIOB
#define	GPIO_SSEG_DATA GPIOB
#define	GPIO_SSEG_LATCH GPIOB

#define	PIN_SSEG_CLK GPIO_PIN_5
#define	PIN_SSEG_DATA GPIO_PIN_6
#define	PIN_SSEG_LATCH GPIO_PIN_7

const uint8_t ascii_to_sseg[] = {
	0x00, // ' '
	0x08, // '!' used for visual -2
	0x14, // '"' used for visual -1
	0x40, // '#' used for visual  0
	0x22, // '$' used for visual  1
	0x01, // '%' used for visual  2
	0x46, // '&'
	0x20, // '''
	0x29, // '('
	0x0B, // ')'
	0x21, // '*'
	0x70, // '+'
	0x10, // ','
	0x40, // '-'
	0x80, // '.'
	0x92, // '/' used for error
	0x3F, // '0'
	0x06, // '1'
	0x5B, // '2'
	0x4F, // '3'
	0x66, // '4'
	0x6D, // '5'
	0x7D, // '6'
	0x07, // '7'
	0x7F, // '8'
	0x6F, // '9'
	0x09, // ':'
	0x0D, // ';'
	0x61, // '<'
	0x48, // '='
	0x43, // '>'
	0xD3, // '?'
	0x5F, // '@'
	0x77, // 'A'
	0x7C, // 'B'
	0x39, // 'C'
	0x5E, // 'D'
	0x79, // 'E'
	0x71, // 'F'
	0x3D, // 'G'
	0x74, // 'H'
	0x30, // 'I'
	0x1E, // 'J'
	0x75, // 'K'
	0x38, // 'L'
	0x15, // 'M'
	0x54, // 'N'
	0x5C, // 'O'
	0x73, // 'P'
	0x67, // 'Q'
	0x50, // 'R'
	0x6D, // 'S'
	0x78, // 'T'
	0x3E, // 'U'
	0x1C, // 'V'
	0x2A, // 'W'
	0x76, // 'X'
	0x6E, // 'Y'
	0x5B, // 'Z'
	0x39, // '['
	0x64, // '\'
	0x0F, // ']'
	0x23, // '^'
	0x08, // '_'
};

static void sreg_sseg_init(void)
{
	HAL_GPIO_WritePin(GPIO_SSEG_CLK, PIN_SSEG_CLK, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIO_SSEG_DATA, PIN_SSEG_DATA, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIO_SSEG_LATCH, PIN_SSEG_LATCH, GPIO_PIN_RESET);
}

static void sreg_sseg_latch(void)
{
	HAL_GPIO_WritePin(GPIO_SSEG_LATCH, PIN_SSEG_LATCH, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIO_SSEG_LATCH, PIN_SSEG_LATCH, GPIO_PIN_RESET);
}

static void sreg_sseg_shift_byte(uint8_t regval)
{
	for (int8_t i = 7; i >= 0; i--) {
	//for (int8_t i = 0; i < 8; i++) {
		HAL_GPIO_WritePin(GPIO_SSEG_DATA, PIN_SSEG_DATA, ((regval >> i) & 1) ? GPIO_PIN_SET : GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIO_SSEG_CLK, PIN_SSEG_CLK, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIO_SSEG_CLK, PIN_SSEG_CLK, GPIO_PIN_RESET);
	}
}

static uint8_t sseg_to_sreg(char character, bool dot)
{
	uint8_t regval;
	uint8_t ascii_lut_idx = character - ASCII_LUT_START;
	if (character >= 'a') {
		ascii_lut_idx -= ('a' - 'A');
	}
	if (ASCII_LUT_START <= character && sizeof(ascii_to_sseg) > ascii_lut_idx){
		regval = (ascii_to_sseg[ascii_lut_idx]);
	}
	else {
		regval = ascii_to_sseg[ASCII_LUT_ERROR - ASCII_LUT_START];
	}

	if (dot) {
		regval |= (1 << 7);
	}

	return regval;
}


void sseg_set_all(Sseg_content contents[], uint8_t num_contents)
{
	uint8_t nr_sseg = 0;
	for (uint8_t nr_content = 0; nr_content < num_contents; nr_content++) {
		uint8_t nr_content_reversed = num_contents - 1 - nr_content;
		uint8_t num_chars = strlen(contents[nr_content_reversed].value);
		for (uint8_t nr_char = 0; nr_char < num_chars; nr_char ++) {
			uint8_t nr_char_reversed = num_chars - 1 - nr_char;
			uint8_t character = contents[nr_content_reversed].value[nr_char_reversed];
			bool dot = contents[nr_content_reversed].dot && (num_chars - 1 == nr_char_reversed);
			uint8_t regval = sseg_to_sreg(character, dot);
			sreg_sseg_shift_byte(regval);
			nr_sseg ++;
		}
	}
	sreg_sseg_latch();
}

void sseg_set_string(char *str)
{
	uint8_t len = strlen(str);

	char str_cp[len + 1];

	memcpy(str_cp, str, len + 1);

    char *lines[4] = {"", "", "", ""};

    lines[0] = str_cp;

    uint8_t nr_line = 1;

    for (uint8_t i = 0; i < len; i++) {
        if ('\n' == str_cp[i]) {
        	str_cp[i] = '\0';
            lines[nr_line] = &str_cp[i + 1];
            nr_line ++;
            if (3 < nr_line) {
            	break;
            }
        }
    }

	for (int8_t nr_sseg = 33; nr_sseg >= 0; nr_sseg --) {
		uint8_t nr_line;
		uint8_t nr_char;
		if (23 < nr_sseg) {
			nr_line = 3;
			nr_char = nr_sseg - 24;
		}
		else {
			nr_line = nr_sseg % 3;
			nr_char = nr_sseg / 3;
		}
		char character = (nr_char < strlen(lines[nr_line]) ? lines[nr_line][nr_char] : ' ');
		uint8_t regval = sseg_to_sreg(character, false);
		sreg_sseg_shift_byte(regval);
	}

	sreg_sseg_latch();
}

void sseg_init(void)
{
	sreg_sseg_init();
}
