/*
 * settings.c
 *
 *  Created on: Jan 1, 2024
 *      Author: jojo
 */

#include "settings.h"

#include "sseg.h"
#include "logging.h"

#include "spi.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#define NUM_SSEGS (sizeof(sseg_configs) / sizeof(*sseg_configs))

typedef enum {
	DISPMODE_NUMERIC,
	DISPMODE_CHROMATIC,
	DISPMODE_SCALEMODE,
	DISPMODE_VISUAL,
	DISPMODE_ENUMERATE,
	DISPMODE_VOICEMODE,
} Display_mode;


enum SpiCommand {
	CMD_SETSETTING = 1,
	CMD_REQSETTING_VALUE,
	CMD_REQSETTING_COMPLETE,
	CMD_REPLY_SETTING_VALUE,
	CMD_REPLY_SETTING_COMPLETE,
};

typedef struct {
	int32_t value;
	int32_t val_min;
	int32_t val_max;
	Display_mode display;
} Setting;

typedef struct {
	uint8_t setting;
	uint8_t num_digits;
} Sseg_config;

static int8_t setting_selected = -1;

static char *strings_chromatic[] = {
		"C",
		"CS",
		"D",
		"DS",
		"E",
		"F",
		"FS",
		"G",
		"GS",
		"A",
		"AS",
		"B"
};

static char *strings_scalemodes[] = {
		"EQ",
		"WE",
		"ME",
		"JU",
		"AU",
};

static char *strings_voicemodes[] = {
		"OU",
		"M1",
		"M2",
		"M3",
		"M4",
		"J1",
		"J2",
};

static Sseg_config sseg_configs[] = {
	  {
			  .setting = 0,
			  .num_digits = 1,
	  },
	  {
			  .setting = 1,
			  .num_digits = 1,
	  },
	  {
			  .setting = 2,
			  .num_digits = 1,
	  },
	  {
			  .setting = 3,
			  .num_digits = 1,
	  },
	  {
			  .setting = 4,
			  .num_digits = 1,
	  },
	  {
			  .setting = 5,
			  .num_digits = 1,
	  },
	  {
			  .setting = 6,
			  .num_digits = 1,
	  },
	  {
			  .setting = 7,
			  .num_digits = 1,
	  },
	  {
			  .setting = 8,
			  .num_digits = 1,
	  },
	  {
			  .setting = 9,
			  .num_digits = 1,
	  },
	  {
			  .setting = 10,
			  .num_digits = 1,
	  },
	  {
			  .setting = 11,
			  .num_digits = 1,
	  },
	  {
			  .setting = 12,
			  .num_digits = 1,
	  },
	  {
			  .setting = 13,
			  .num_digits = 1,
	  },
	  {
			  .setting = 14,
			  .num_digits = 1,
	  },
	  {
			  .setting = 15,
			  .num_digits = 1,
	  },
	  {
			  .setting = 16,
			  .num_digits = 1,
	  },
	  {
			  .setting = 17,
			  .num_digits = 1,
	  },
	  {
			  .setting = 18,
			  .num_digits = 1,
	  },
	  {
			  .setting = 19,
			  .num_digits = 1,
	  },
	  {
			  .setting = 20,
			  .num_digits = 1,
	  },
	  {
			  .setting = 21,
			  .num_digits = 1,
	  },
	  {
			  .setting = 22,
			  .num_digits = 1,
	  },
	  {
			  .setting = 23,
			  .num_digits = 1,
	  },
	  {
			  .setting = 24,
			  .num_digits = 2,
	  },
	  {
			  .setting = 25,
			  .num_digits = 2,
	  },
	  {
			  .setting = 26,
			  .num_digits = 2,
	  },
	  {
			  .setting = 27,
			  .num_digits = 2,
	  },
	  {
			  .setting = 28,
			  .num_digits = 2,
	  },
};

static Setting settings[NUM_SETTINGS] = {};

static bool settings_clamp(uint8_t setting) {
	bool res = false;

	if (settings[setting].value < settings[setting].val_min) {
		settings[setting].value = settings[setting].val_min;
		res = true;
	}
	else if (settings[setting].value > settings[setting].val_max) {
		settings[setting].value = settings[setting].val_max;
		res = true;
	}

	return res;
}

static void settings_numeric_to_string(int32_t val_numeric, char val_string[], uint8_t num_digits, Display_mode dispmode) {
	switch (dispmode) {
	case DISPMODE_NUMERIC: {
		snprintf(val_string, num_digits + 1, "%*d", num_digits, (int16_t)val_numeric);
	} break;
	case DISPMODE_CHROMATIC: {
		if (0 <= val_numeric && (sizeof(strings_chromatic) / sizeof(*strings_chromatic)) > val_numeric) {
			snprintf(val_string, num_digits + 1, "%-*s", num_digits, strings_chromatic[val_numeric]);
		}
		else {
			snprintf(val_string, num_digits + 1, "%*c", num_digits, ASCII_LUT_ERROR);
		}
	} break;
	case DISPMODE_SCALEMODE: {
		if (0 <= val_numeric && (sizeof(strings_scalemodes) / sizeof(*strings_scalemodes)) > val_numeric) {
			snprintf(val_string, num_digits + 1, "%-*s", num_digits, strings_scalemodes[val_numeric]);
		}
		else {
			snprintf(val_string, num_digits + 1, "%*c", num_digits, ASCII_LUT_ERROR);
		}
	} break;
	case DISPMODE_VISUAL: {
		if (-2 <= val_numeric && 2 >= val_numeric) {
			snprintf(val_string, num_digits + 1, "%-*c", num_digits, ASCII_LUT_VISUAL_START + (char)(val_numeric + 2));
		}
		else {
			snprintf(val_string, num_digits + 1, "%*c", num_digits, ASCII_LUT_ERROR);
		}
	} break;
	case DISPMODE_ENUMERATE: {
		if (0 <= val_numeric) {
			snprintf(val_string, num_digits + 1, "%*d", num_digits, (int16_t)(val_numeric + 1));
		}
		else {
			snprintf(val_string, num_digits + 1, "%*s", num_digits, "----");
		}
	} break;
	case DISPMODE_VOICEMODE: {
		if (0 <= val_numeric && (sizeof(strings_voicemodes) / sizeof(*strings_voicemodes)) > val_numeric) {
			snprintf(val_string, num_digits + 1, "%-*s", num_digits, strings_voicemodes[val_numeric]);
		}
		else {
			snprintf(val_string, num_digits + 1, "%*c", num_digits, ASCII_LUT_ERROR);
		}
	} break;
	default: {
		snprintf(val_string, num_digits + 1, "%*c", num_digits, ASCII_LUT_ERROR);
	}
	}
}

static bool settings_set_forced(uint8_t setting, int32_t value) {
	bool ret = false;

	if (value != settings[setting].value) {
		ret = true;
		settings[setting].value = value;
	}

	return ret;
}


bool settings_select(int8_t setting) {
	bool res = true;

	if (NUM_SETTINGS > setting && -1 <= setting) {
		setting_selected = setting;
		res = true;
	}

	return res;
}

bool settings_set(uint8_t setting, int32_t value) {
	int32_t value_previous = settings[setting].value;

	if (NUM_SETTINGS > setting) {

		if (26 == setting && value > settings[27].value) {
			settings[setting].value = settings[27].value;
		}
		else if (27 == setting && value > settings[28].value) {
			settings[setting].value = settings[28].value;
		}
		else if (27 == setting && value < settings[26].value) {
			settings[setting].value = settings[26].value;
		}
		else if (28 == setting && value < settings[27].value) {
			settings[setting].value = settings[27].value;
		}
		else {
			settings[setting].value = value;
		}
		settings_clamp(setting);
	}

	return (settings[setting].value != value_previous);
}

bool settings_increment(uint8_t setting) {
	return (settings_set(setting, settings[setting].value + 1));
}

bool settings_decrement(uint8_t setting) {
	return (settings_set(setting, settings[setting].value - 1));
}

bool settings_increment_current(void) {
	bool res = false;

	if (0 <= setting_selected) {
		res = settings_increment(setting_selected);
	}

	return res;
}

bool settings_decrement_current(void) {
	bool res = false;

	if (0 <= setting_selected) {
		res = settings_decrement(setting_selected);
	}

	return res;
}

bool settings_set_current(int32_t value) {
	bool res = false;

	if (0 <= setting_selected) {
		res = settings_set(setting_selected, value);
	}

	return res;
}

int32_t settings_get(uint8_t setting) {
	if (NUM_SETTINGS > setting) {
		return settings[setting].value;
	}
	else {
		return 0;
	}
}

int32_t settings_get_current(void) {
	if (0 <= setting_selected) {
		return settings_get(setting_selected);
	}
	else {
		return 0;
	}
}

void settings_show(void) {
	Sseg_content contents[NUM_SSEGS];

	for (uint8_t nr_sseg = 0; nr_sseg < NUM_SSEGS; nr_sseg++) {
		settings_numeric_to_string(settings[sseg_configs[nr_sseg].setting].value, contents[nr_sseg].value, sseg_configs[nr_sseg].num_digits, settings[sseg_configs[nr_sseg].setting].display);
		contents[nr_sseg].dot = (sseg_configs[nr_sseg].setting == setting_selected);
	}

	sseg_set_all(contents, sizeof(contents) / sizeof(*contents));
}

bool settings_send(uint8_t setting) {
	bool res = true;
	if (NUM_SETTINGS > setting) {
		int32_t value = settings[setting].value;
		uint8_t data[] = {CMD_SETSETTING, setting, (value >> 24) & 0xff, (value >> 16) & 0xff, (value >> 8) & 0xff, value & 0xff};
		HAL_StatusTypeDef spi_status = HAL_SPI_Transmit(&hspi1, data, sizeof(data), 100);
		if (HAL_OK != spi_status) {
		  res = false;
		}
	}
	else {
		res = false;
	}
	return res;
}

bool settings_send_current(void) {
	if (0 <= setting_selected) {
		return settings_send(setting_selected);
	}
	else {
		return 0;
	}
}

bool settings_obtain_value_one(uint8_t setting) {
	bool ret = true;

	uint8_t data_tx[] = {CMD_REQSETTING_VALUE, setting, 0, 0, 0, 0};
	HAL_StatusTypeDef spi_status = HAL_SPI_Transmit(&hspi1, data_tx, sizeof(data_tx), 100);
	if (HAL_OK != spi_status) {
		ret = false;
		log_warning("Sending request for setting %u yielded error code %d", setting, spi_status);
	}
	else {
		HAL_Delay(20);
		uint8_t data_rx[sizeof(int32_t) + 1];
		spi_status = HAL_SPI_Receive(&hspi1, data_rx, sizeof(data_rx), 100);
		if (HAL_OK != spi_status) {
			ret = false;
			log_warning("Receiving response for setting %u yielded error code %d", setting, spi_status);
		}
		else if (CMD_REPLY_SETTING_VALUE != data_rx[0]) {
			log_warning("Received unmatching response status: %u", data_rx[0]);
			ret = false;
		}
		else {
			int32_t value_rec = (data_rx[1] << 24) | (data_rx[2] << 16) | (data_rx[3] << 8) | data_rx[4];
			settings_set_forced(setting, value_rec);
			log_debug("Obtained value for setting %u: %d", setting, value_rec);
		}
	}

	return ret;
}

bool settings_obtain_value_all(void) {
	bool ret = true;

	for (uint8_t nr_setting = 0; (nr_setting < NUM_SETTINGS) && ret; nr_setting ++) {
		ret = settings_obtain_value_one(nr_setting);
		HAL_Delay(10);
	}

	return ret;
}

bool settings_obtain_complete_one(uint8_t setting) {
	bool ret = true;

	uint8_t data_tx[] = {CMD_REQSETTING_COMPLETE, setting, 0, 0, 0, 0};
	HAL_StatusTypeDef spi_status = HAL_SPI_Transmit(&hspi1, data_tx, sizeof(data_tx), 100);
	if (HAL_OK != spi_status) {
		ret = false;
		log_warning("Sending request for setting %u yielded error code %d", setting, spi_status);
	}
	else {
		HAL_Delay(20);
		uint8_t data_rx[sizeof(Setting) + 1];
		spi_status = HAL_SPI_Receive(&hspi1, data_rx, sizeof(data_rx), 100);
		if (HAL_OK != spi_status) {
			ret = false;
			log_warning("Receiving response for setting %u yielded error code %d", setting, spi_status);
		}
		else if (CMD_REPLY_SETTING_COMPLETE != data_rx[0]) {
			log_warning("Received unmatching response status: %u", data_rx[0]);
			ret = false;
		}
		else {
			Setting setting_rec = *((Setting*)(data_rx + 1));
			memcpy(&settings[setting], &setting_rec, sizeof(setting_rec));
			log_debug("Obtained value for setting %u: %d", setting, setting_rec.value);
		}
	}

	return ret;
}

bool settings_obtain_complete_all(void) {
	bool ret = true;

	for (uint8_t nr_setting = 0; (nr_setting < NUM_SETTINGS) && ret; nr_setting ++) {
		ret = settings_obtain_complete_one(nr_setting);
		HAL_Delay(10);
	}

	return ret;
}
