/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f3xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define BTN_IN_Pin GPIO_PIN_0
#define BTN_IN_GPIO_Port GPIOA
#define BTN_IN_EXTI_IRQn EXTI0_IRQn
#define KNOB_A_Pin GPIO_PIN_8
#define KNOB_A_GPIO_Port GPIOA
#define KNOB_A_EXTI_IRQn EXTI9_5_IRQn
#define BTN_CLK_Pin GPIO_PIN_9
#define BTN_CLK_GPIO_Port GPIOA
#define BTN_DATA_Pin GPIO_PIN_10
#define BTN_DATA_GPIO_Port GPIOA
#define BTN_LATCH_Pin GPIO_PIN_11
#define BTN_LATCH_GPIO_Port GPIOA
#define KNOB_B_Pin GPIO_PIN_12
#define KNOB_B_GPIO_Port GPIOA
#define KNOB_B_EXTI_IRQn EXTI15_10_IRQn
#define SSEG_CLK_Pin GPIO_PIN_5
#define SSEG_CLK_GPIO_Port GPIOB
#define SSEG_DATA_Pin GPIO_PIN_6
#define SSEG_DATA_GPIO_Port GPIOB
#define SSEG_LATCH_Pin GPIO_PIN_7
#define SSEG_LATCH_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
