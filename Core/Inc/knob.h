/*
 * knob.h
 *
 *  Created on: Jan 1, 2024
 *      Author: jojo
 */

#include <stdint.h>
#include <stdbool.h>

#ifndef INC_KNOB_H_
#define INC_KNOB_H_

#define	GPIO_KNOB_A GPIOA
#define	GPIO_KNOB_B GPIOA

#define	PIN_KNOB_A GPIO_PIN_8
#define	PIN_KNOB_B GPIO_PIN_12


int8_t evaluate_knob(bool a, bool b);


#endif /* INC_KNOB_H_ */
