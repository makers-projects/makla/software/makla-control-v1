/*
 * settings.h
 *
 *  Created on: Jan 1, 2024
 *      Author: jojo
 */

#ifndef INC_SETTINGS_H_
#define INC_SETTINGS_H_

#include <stdint.h>
#include <stdbool.h>

#define NUM_SETTINGS 29

bool settings_select(int8_t setting);

bool settings_set(uint8_t setting, int32_t value);

bool settings_increment(uint8_t setting);

bool settings_decrement(uint8_t setting);

bool settings_increment_current(void);

bool settings_decrement_current(void);

bool settings_set_current(int32_t value);

int32_t settings_get(uint8_t setting);

int32_t settings_get_current(void);

void settings_show(void);

bool settings_send(uint8_t setting);

bool settings_send_current(void);

bool settings_obtain_value_one(uint8_t setting);

bool settings_obtain_value_all(void);

bool settings_obtain_complete_one(uint8_t setting);

bool settings_obtain_complete_all(void);

#endif /* INC_SETTINGS_H_ */
