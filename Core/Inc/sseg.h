/*
 * sseg.h
 *
 *  Created on: Dec 3, 2023
 *      Author: jojo
 */

#ifndef INC_SSEG_H_
#define INC_SSEG_H_


#include <stdint.h>
#include <stdbool.h>


#define ASCII_LUT_START ' '
#define ASCII_LUT_VISUAL_START '!'
#define ASCII_LUT_ERROR '/'


typedef struct {
	char value[7];
	bool dot;
} Sseg_content;


void sseg_set_all(Sseg_content contents[], uint8_t num_contents);

void sseg_set_string(char *str);

void sseg_init(void);


#endif /* INC_SSEG_H_ */
