/*
 * logging.h
 *
 *  Created on: Nov 16, 2023
 *      Author: jojo
 */

#ifndef INC_LOGGING_H_
#define INC_LOGGING_H_

void log_baseline(const char *formatstr, ...);

void log_error(const char *formatstr, ...);

void log_warning(const char *formatstr, ...);

void log_debug(const char *formatstr, ...);

void log_noisy(const char *formatstr, ...);

#endif /* INC_LOGGING_H_ */
