/*
 * buttons.h
 *
 *  Created on: Dec 3, 2023
 *      Author: jojo
 */

#ifndef INC_BUTTONS_H_
#define INC_BUTTONS_H_


#include <stdint.h>

#define	GPIO_BTN_CLK GPIOA
#define	GPIO_BTN_DATA GPIOA
#define	GPIO_BTN_LATCH GPIOA
#define	GPIO_BTN_INPUT GPIOA

#define	PIN_BTN_CLK GPIO_PIN_9
#define	PIN_BTN_DATA GPIO_PIN_10
#define	PIN_BTN_LATCH GPIO_PIN_11
#define	PIN_BTN_INPUT GPIO_PIN_0

void btn_init(void);

uint32_t btn_read(void);


#endif /* INC_BUTTONS_H_ */
